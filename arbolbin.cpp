
#include <iostream>
#include <fstream>
#include <unistd.h>
#include "arbolbin.h"

using namespace std;

// Constructor de la clase
arbolbin::arbolbin(){
}

// Funcion que crea el nodo
void arbolbin::agregar(int dato){
    Nodo *num = new Nodo;
    num -> dato = dato;
    num -> left = NULL;
    num -> right = NULL;
    this -> root = insertar(this -> root, num);
}

// Metodo que obtiene el valor de root
Nodo *arbolbin::obtener(){
    return this -> root;
}

// Funcion que busca un nodo en especifico
void arbolbin::buscar(Nodo *num, int out){
    if(out < num -> dato){
        if(num -> left == NULL){
            cout << "El numero buscado no se ha encontrado." << endl;
        }else{
            buscar(num -> left, out);
        }
    }else{
        if(out > num -> dato){
            if(num -> right == NULL){
                cout << "El numero buscado no se ha encontrado." << endl;
            }else{
                buscar(num -> right, out);
            }
        }else{
            cout << "El numero que desea eliminar ha sido encontrado." << endl;
        }
    }
}

// Funcion que insertar un nodo en el arbol
Nodo *arbolbin::insertar(Nodo *num, Nodo *alt){
    if(num == NULL){
        num = alt;
        cout << "Numero insertado." << endl << endl;
    }else{
        if(alt -> dato < num -> dato){
            num -> left = insertar(num -> left, alt);
        }else if(alt -> dato > num -> dato){
            num -> right = insertar(num -> right, alt);
        }else{
            cout << "Error, el numero que desea insertar ya se encuentra en el arbol." << endl << endl;
        }
    }
    return num;
}

// Funcion que elimina un nodo del arbol
void arbolbin::eliminar(Nodo *num, int dato){
    Nodo *dat2;
    if(num != NULL){
        if(dato < num -> dato){
            eliminar(num -> left, dato);
        }else{
            if(dato > num -> dato){
                eliminar(num -> right, dato);
            }else{
                Nodo *num2 = num;
                if(num2 -> right == NULL){
                    num = num2 -> left;
                }else{
                    if(num2 -> left == NULL){
                        num = num2 -> right;
                    }else{
                        Nodo *dat1 = num -> left;
                        bool eliminado = false;
                        while(dat1 -> right != NULL){
                            dat2 = dat1;
                            dat1 = dat1 -> right;
                            eliminado = true;
                        }
                        num -> dato = dat1 -> dato;
                        num2 = dat1;
                        if(eliminado == true){
                            dat2 -> right = dat1 -> left;
                        }else{
                            num -> left = dat1 -> left;
                        }
                    }
                }
                delete num2;
            }
        }
    }else{
        cout << "Error, el numero que desea eliminar no existe." << endl;
    }
}

// Metodo de impresion en Preorden
void arbolbin::metodoPreorden(Nodo *num){
    if(num != NULL){
        cout << "[" << num -> dato << "]" << " - ";
        metodoPreorden(num -> left);
        metodoPreorden(num -> right);
    }
}

// Metodo de impresion en Inorden
void arbolbin::metodoInorden(Nodo *num){
    if(num != NULL){
        metodoInorden(num -> left);
        cout << "[" << num -> dato << "]" << " - ";
        metodoInorden(num -> right);
    }
}

// Metodo de impresion en Posorden
void arbolbin::metodoPosorden(Nodo *num){
    if(num != NULL){
        metodoPosorden(num -> left);
        metodoPosorden(num -> right);
        cout << "[" << num -> dato << "]" << " - ";
    }
}

// Funcion que genera el Grafo del arbol binario
void arbolbin::generarGrafo(Nodo *num){
    ofstream archivo("grafo.txt", ios::out);
    archivo << "digraph G {" << endl;
    archivo << "node [style=filled fillcolor=green];" << endl;
    arbol(num, archivo);
    archivo << "}" << endl;
    archivo.close();

    system("dot -Tpng -ografo.png grafo.txt &");
    system("eog grafo.png &");
}

// Funcion que lee todos los datos del arbol
void arbolbin::arbol(Nodo *num, ofstream & archivo){
    if(num != NULL){
        if(num -> left != NULL){
            archivo << num -> dato << " -> " << num -> left -> dato << ";" << endl;
        }else{
            archivo << '"' << num -> dato << "i" << '"' << "[shape=point];"<< endl;
            archivo << num -> dato << " -> " << '"' << num -> dato << "i" << '"' << ";"<< endl;
        }
        if(num -> right != NULL){
            archivo << num -> dato << " -> " << num -> right -> dato << ";"<< endl;
        }else{
            archivo << '"' << num -> dato << "d" << '"' << "[shape=point];"<< endl;
            archivo << num -> dato << " -> " << '"' << num -> dato << "d" << '"' << ";"<< endl;
        }
        arbol(num -> left, archivo);
        arbol(num -> right, archivo);
    }
}