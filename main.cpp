
#include <iostream>
#include <fstream>
#include <unistd.h>
#include "arbolbin.h"

// Funcion que muestra las opciones al usuario en la terminal
void opciones(){
    cout << endl;
    cout << "Elija una de las siguientes opciones: " << endl << endl;
    cout << "Insertar numero. -------------------(1)" << endl;
    cout << "Eliminar numero buscado. -----------(2)" << endl;
    cout << "Modificar un elemento buscado. -----(3)" << endl;
    cout << "Mostrar el contenido del arbol. ----(4)" << endl;
    cout << "Generar el grafo correspondiente. --(5)" << endl;
    cout << "Finalizar el Programa. -------------(0)" << endl;
}

// Funcion main del Programa
int main(int argc, char* argv[]){
    int dato = 1;
    int del = 0;
    bool flag = true;
    string op, n;
    
    arbolbin *arbolBin = new arbolbin();
    Nodo *root = new Nodo();

    while(flag){
        opciones();
        getline(cin, op);
        system("clear");

        switch (stoi(op))
        {
        // Finalizar el Programa.
        case 0:
            cout << "El programa ha Finalizado." << endl;
            flag = false;
            break;

        // Insertar numero.
        case 1:
            cout << "Ingrese el numero que desea agregar: ";
            getline(cin, n);
            dato = stoi(n);
            arbolBin->agregar(dato);
            break;

        // Eliminar numero buscado.
        case 2:
            root = arbolBin -> obtener();
            if(root != NULL){
                cout << "Ingrese el numero que desea eliminar: " << endl;
                arbolBin -> metodoInorden(root);
                cout << "---------" << endl;
                getline(cin, n);
                del = stoi(n);
                arbolBin -> eliminar(arbolBin -> obtener(), del);
                del = 0;
            }else{
                cout << "ERROR, no existe ningun elemento en el arbol." << endl << endl;
            }
            break;

        // Modificar un elemento buscado.
        case 3:
            root = arbolBin -> obtener();
            if(root != NULL){
                cout << "Ingrese el numero que desea modificar: " << endl;
                arbolBin -> metodoInorden(root);
                cout << "---------" << endl;
                getline(cin, n);
                arbolBin -> eliminar(arbolBin -> obtener(), stoi(n));
                cout << "Ingrese el numero que reemplaza al modificado: " << endl;
                getline(cin, n);
                dato = stoi(n);
                arbolBin -> agregar(dato);
            }else{
                cout << "ERROR, no existe ningun elemento en el arbol." << endl << endl;
            }
            break;

        // Mostrar el contenido del arbol.    
        case 4:
            root = arbolBin -> obtener();
            if(root != NULL){
                cout << "Se mostrara el contenido del arbol en Preorden, Inorden y Posorden: " << endl << endl;
                cout << "Metodo Preorden: " << endl;
                arbolBin -> metodoPreorden(root);
                cout << endl;
                cout << "Metodo Inorden: " << endl;
                arbolBin -> metodoInorden(root);
                cout << endl;
                cout << "Metodo Posorden: " << endl;
                arbolBin -> metodoPosorden(root);
                cout << endl;
            }else{
                cout << "ERROR, no existe ningun elemento en el arbol." << endl << endl;
            }
            break;

        // Generar el grafo correspondiente.    
        case 5:
            root = arbolBin -> obtener();
            if(root != NULL){
                arbolBin -> generarGrafo(root);
            }else{
                cout << "ERROR, no existe ningun elemento en el arbol." << endl << endl;
            }
            break;

        // En caso de que no se elija una opcion entre 0 y 5.    
        default:
            cout << "ERROR, Ingrese una opcion entre 0 y 5" << endl;
            break;
        }
    }
    delete(root);
    delete(arbolBin);
    return 0;
}