
Guia 4 AED
Guia numero 4 Algortimos y Estructura de Datos, Unidad 3
Autor: Benjamin Martin
Fecha: 11 de Octubre

El presente programa permite generar un arbol binario de busqueda que contenga solo numeros enteros, verifiando que estos no se repitan entre si.
Las acciones que realiza este programa son:
    - Insertar un numero al arbol binario.
    - Eliminar un numero buscado del arbol binario.
    - Modificar un numero buscado del arbol binario.
    - Mostrar el contenido del arbol binario en los metodos Preorden, Inorden y Posorden.
    - Generar el Grafo del arbol binario.
Las cuales se presentan al usuario en un Menu, en la terminal.

Los pasos para Compilar y Ejecutar este programa son:
    1. Iniciar terminal.
    2. Ingresar el comando make (para compilar).
    3. ingresar el comando ./main para ejecutar el programa.