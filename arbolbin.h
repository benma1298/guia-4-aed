
#ifndef ARBOLBIN_H
#define ARBOLBIN_H
#include <iostream>
#include <fstream>
#include <unistd.h>

using namespace std;

// Estructura Nodo
typedef struct _Nodo{
    int dato;
    struct _Nodo *right;
    struct _Nodo *left;
} Nodo;

class arbolbin{
    private:
    Nodo *root = NULL;

    public:
    // Constructor de clase
    arbolbin();

    // Metodos requeridos del programa
    Nodo *obtener();
    Nodo* insertar(Nodo *num, Nodo *alt);
    void agregar(int dato);
    void buscar(Nodo *num, int out);
    void eliminar(Nodo *num, int dato);
    void generarGrafo(Nodo *num);
    void arbol(Nodo *num, ofstream &archivo);

    // Metodos de orden
    void metodoPreorden(Nodo *num);
    void metodoInorden(Nodo *num);
    void metodoPosorden(Nodo *num);
};

#endif